var url = "./images/1x1/";
jQuery(document).ready(function() {
    loadMap();
});
function loadMap() {
    jQuery("#loading").fadeIn();
    jQuery("#blur-loading").addClass('blur');
    var loc = window.location.href.split('#')[1];
    if(loc == undefined) {
        loc = "grad";
        window.location.hash = "grad";
    }
    var mapImages = {}, overLay = {};
    jQuery.ajax({
        url: "./item-data.json",
        method: "GET",
        dataType: "json"
    }).done(function(itemData) {
       var mappingOrder = [];
        for(i in itemData) {
            mappingOrder.push(i);
        }
        mappingOrder.sort();
        mapImages = createItemProfiles(itemData, mappingOrder);
        duplicateCheck = {};
        jQuery.ajax({
            url: "./"+loc+".json",
            method: "GET",
            dataType: "json"                
        }).done(function(data) {
            createMap(data.name, function(map) {
                jQuery(data.data).each(function(k,v) {
                    if(v.coordinates[0] !== 0 && v.coordinates[1] !== 0) {
                        overLay[v.type] = new ol.Overlay({
                            position: v.coordinates,
                            element: mapImages[v.type],
                            id: v.type
                        });
                        map.addOverlay(overLay[v.type]);	
                    }	
                    jQuery("#loading").fadeOut();
                    jQuery("#blur-loading").removeClass('blur');
                
                }); 
                loadListeners(map);              
            });
        });
    });
}
function loadListeners(map) {
    jQuery(".menu-hover").hover(function() {
        var id = jQuery(this).attr('data-id');
        jQuery("#map").find('.icon-img[data-id='+id+']').addClass('do-glow');
    }, function() {
        var id = jQuery(this).attr('data-id');
        jQuery("#map").find('.icon-img[data-id='+id+']').removeClass('do-glow');
    });
    jQuery(".menu-hover").click(function() {
        var id = jQuery(this).attr('data-id');  
        var position = map.getOverlayById(id).getPosition();
        map.getView().setCenter(position);
    });
    jQuery("#map").click(function() {
        console.log(jQuery("#mouse-position").html());
    });  
    jQuery("#selectMap a").click(function() {
        var newLoc = jQuery(this).attr('href').split('#')[1];
        jQuery(this).attr('disabled', true);
        window.location.hash = newLoc;
        jQuery("#map").find('.ol-viewport').remove();
        loadMap();
    });
    jQuery("#search").click(function() {
        jQuery("#search").val('');
        resetSearchPreferences();
    });
    jQuery("#search-cancel").click(function() {
        jQuery("#search").val('');
        resetSearchPreferences();
    });
    jQuery("#search").on('keyup', function() {
        jQuery("#search-no-results").hide();
        var search = jQuery("#search").val().toUpperCase();
        var hasAtLeastOne = false;
        jQuery("#legend").find("ul li").each(function(k,v) {
            var el = jQuery(v).find('a');
            el.parent().show();
            var text = el.text().toUpperCase();
            var result = text.includes(search);
            if(!result) {
                el.parent().hide();
            } else {
                hasAtLeastOne = true;
            }
        });
        if(!hasAtLeastOne) {
            jQuery("#search-no-results").fadeIn(100);
        } else {
            jQuery("#search-cancel").fadeIn(100);
        }
    });
}

function resetSearchPreferences() {
    jQuery("#legend").find("ul li").each(function(k,v) {
        var el = jQuery(v).find('a');
        el.parent().slideDown(100);
    });
    jQuery("#search-no-results").fadeOut(100);
    jQuery("#search-cancel").fadeOut(100);
    hasAtLeastOne = false;
}

function createMap(imageBg, callBack) {
    var extent = [0, 0, 3840, 2160];
    var overLay = {};
    var projection = new ol.proj.Projection({
        code: 'image',
        units: 'pixels',
        extent: extent
    });
    var mousePosition = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(4),
        projection: projection,
        className: 'custom-mouse-position',
        target: document.getElementById('mouse-position'),
        undefinedHTML: '&nbsp;'
    })
    var map = new ol.Map({
        layers: [
            new ol.layer.Image({
                source: new ol.source.ImageStatic({
                    url: imageBg,
                    projection: projection,
                    imageExtent: extent
                })
            })
        ],
        controls: ol.control.defaults ().extend([mousePosition]),
        target: 'map',
        view: new ol.View({
            projection: projection,
            center: ol.extent.getCenter(extent),
            zoom: 4,
            maxZoom: 4,
            minZoom: 2
        })
    });
    callBack(map);
}

function createItemProfiles(itemData, mappingOrder) {
    var images = {};
    for (mapNameIdx in mappingOrder) {
        var item = mappingOrder[mapNameIdx];
        var that = itemData[item];
        console.log('that', that);
        // Create legend list
        jQuery('#legend ul').append(
            jQuery('<li />', {class: 'menu-hover', 'data-id': item}).append(
                jQuery('<a />', { html: itemData[item].text, href: "javascript:void(0)"}).append(
                    loadMapImage(item, that.icon, that.backgroundColor)
                )		
            )
        );		

        // Create on-map element
        var title = document.createElement("p");
        var image = loadMapImage(item, that.icon, that.backgroundColor);
        title.appendChild(document.createTextNode(that.text));
        images[item] = document.createElement("div");
        images[item].className="item " + item;
        images[item].appendChild(
            title
        );
        images[item].appendChild(image);
    }
    return images;
}

function loadMapImage(inId, inUrl, bgColor) {
    image = new Image();
    image.src = url + inUrl;
    image.style.backgroundColor = bgColor;
    image.width = "25";
    image.height = "25";
    image.opacity = 0.9;	
    image.className = 'icon-img';
    image.setAttribute('data-id', inId);
    return image;
}